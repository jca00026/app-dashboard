import { Injectable } from '@angular/core';

import { Router } from "@angular/router";
import { auth, firestore } from 'firebase/app';

import { User } from 'firebase';
import { map } from 'rxjs/operators';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';



export interface Ejer{
  Nombre:string,
  Tipo:string,
  Description:string,
  Hist:Array<any>,
  }
  export interface Rutina {
    Nombre: string,
    DiasEntrenar: number,
    DuracionSemanas: number,
  }
  export interface DIA{
    Titulo:string,
    Ejercicios:any,
    Vector:any,
    Numero: number;
  }

@Injectable({
  providedIn: 'root'
})
export class FirestoreService {

  private user: User;
  private usuario;
  constructor(private firestore: AngularFirestore, private afAuth: AngularFireAuth, private router: Router) {
    this.afAuth.authState.subscribe(user => {
      if (user) {
        this.user = user;
        localStorage.setItem('user', JSON.stringify(this.user));
        this.usuario = JSON.parse(localStorage.getItem('user'));
      } else {
        localStorage.setItem('user', null);
      }
    })
    this.usuario = JSON.parse(localStorage.getItem('user'));

  }
  isAuth() {
    return this.afAuth.authState.pipe(map(auth => auth));
  }

  async login(email: string, password: string) {
    await this.afAuth.auth.signInWithEmailAndPassword(email, password);



  }
  async register(email: string, password: string) {
    var result = await this.afAuth.auth.createUserWithEmailAndPassword(email, password)
    this.sendEmailVerification();
  }
  async sendEmailVerification() {
    await this.afAuth.auth.currentUser.sendEmailVerification().then(function () {
    }).catch(function (error) {
    });
    this.crearUser();
    localStorage.setItem("logueado","false");
    this.router.navigate(['']);
  }
  


  async sendPasswordResetEmail(passwordResetEmail: string) {
    return await this.afAuth.auth.sendPasswordResetEmail(passwordResetEmail);
  }
  async logout() {
    await this.afAuth.auth.signOut();
    localStorage.removeItem('user');
    localStorage.setItem("logueado","false");
    this.router.navigate([""]);
  }
  get isLoggedIn(): boolean {
    const user = JSON.parse(localStorage.getItem('user'));
    return user !== null;
  }
  //para google no creo que lo utilice
  async  loginWithGoogle() {
    await this.afAuth.auth.signInWithPopup(new auth.GoogleAuthProvider())
    this.router.navigate(['admin/list']);
  }
  getUsuario() {
    return this.user;
  }

  public generarID(){
    return this.firestore.createId();
  }



  public crearUser() {
    // crear usuario con sus datos
    let data = {
      Email: this.user.email,
      RutinaActual: "",
      Meta: "",
    }
    this.firestore.collection('user').doc(this.user.uid).set(data);
  }
  getDataUser() {
    return this.firestore.collection('user').doc('sCJhsZcYoXWrBdls9GVqKawR3tf1').snapshotChanges();
  }
  // para actualizar por si acaso tiene ya creado 

  // Ejercicios
  public addEjercicios(datos) {
   let data={
    Nombre: datos.nombre,
    Tipo: datos.musculo,
    Description: datos.descripcion,
    Hist:[],
    Ocultar:false,
   }
   let _id=this.firestore.createId();


   this.firestore.collection('user').doc('sCJhsZcYoXWrBdls9GVqKawR3tf1').collection('Ejercicios').doc(_id).set(data);
   return _id;
  }
  public getRefEjercicio(id){
    return this.firestore.collection('user').doc('sCJhsZcYoXWrBdls9GVqKawR3tf1').collection('Ejercicios').doc(id).ref;
  }
  public getEjercicios(){
    return this.firestore.collection('user').doc('sCJhsZcYoXWrBdls9GVqKawR3tf1').collection('Ejercicios').snapshotChanges();
  }
  public getEjercicio(id){
    return this.firestore.collection('user').doc('sCJhsZcYoXWrBdls9GVqKawR3tf1').collection('Ejercicios').doc<Ejer>(id).snapshotChanges();
  }

  public ocultarEjercicio(id){
      let data={
        Ocultar:true,
      }
      return this.firestore.collection('user').doc('sCJhsZcYoXWrBdls9GVqKawR3tf1').collection('Ejercicios').doc(id).update(data);
  
  }



  //HISTORIAL
  public getHirtorial(){
    return this.firestore.collection('user').doc('sCJhsZcYoXWrBdls9GVqKawR3tf1').collection('Historial').snapshotChanges();

  }


  //PESOS
  public getHitorialPesos(){
   return  this.firestore.collection('user').doc('sCJhsZcYoXWrBdls9GVqKawR3tf1').collection('Pesos').snapshotChanges()
  }




  public getRutina(idRutina) {
    return this.firestore.collection('user').doc('sCJhsZcYoXWrBdls9GVqKawR3tf1').collection('Rutinas').doc(idRutina).collection('dias').snapshotChanges();
  }

    public getDatosRutina(idRutina){
  return this.firestore.collection('user').doc('sCJhsZcYoXWrBdls9GVqKawR3tf1').collection('Rutinas').doc<Rutina>(idRutina).snapshotChanges();
    
  }
  //Funcion para consultar un dia especifico;
  public getRutinaDia(idDia, idRutina) {
    return this.firestore.collection('user').doc('sCJhsZcYoXWrBdls9GVqKawR3tf1').collection('Rutinas').doc(idRutina).collection('dias').doc<DIA>(idDia).snapshotChanges();
  }
  public getRutinas(){
    return this.firestore.collection('user').doc('sCJhsZcYoXWrBdls9GVqKawR3tf1').collection('Rutinas').snapshotChanges();
  }
}
