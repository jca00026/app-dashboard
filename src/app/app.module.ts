import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {AngularFireModule} from '@angular/fire'
import {environment} from '../environments/environment'
import { AngularFirestore } from '@angular/fire/firestore';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import {AngularFireAuthModule} from '@angular/fire/auth'

import { ChartsModule } from 'ng2-charts';
import {MatCardModule} from '@angular/material/card';
import { AuthService } from './servicios/auth.service';
import { DashboardComponent } from './componentes/dashboard/dashboard.component';
import { LoginComponent } from './componentes/login/login.component';
import { MatToolbarModule, MatIconModule, MatSidenavModule, MatListModule, MatButtonModule, MatSlideToggleModule, MatDatepickerModule, MatNativeDateModule, MatFormFieldModule, MatDialogModule, MatInputModule, MatSelectModule, MatTabsModule, MatCheckboxModule} from  '@angular/material';
import { PesoComponent } from './componentes/chart/peso/peso.component';
import { BarraComponent } from './componentes/chart/barra/barra.component';
import { AppGuard } from './app.guard';
import { RestablecerContraseniaComponent } from './componentes/restablecer-contrasenia/restablecer-contrasenia.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    LoginComponent,
    PesoComponent,
    BarraComponent,
    RestablecerContraseniaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireAuthModule,
    AngularFireModule.initializeApp(environment.firebase),
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ChartsModule,
    MatToolbarModule,MatCardModule, MatIconModule, MatSidenavModule, MatListModule, MatButtonModule, MatSlideToggleModule, MatDatepickerModule, MatNativeDateModule, MatFormFieldModule, MatDialogModule, MatInputModule, MatSelectModule, MatTabsModule, MatCheckboxModule
  ],
  providers: [AngularFirestore,AuthService,AppGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
