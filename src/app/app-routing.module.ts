import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './componentes/dashboard/dashboard.component';
import { AppGuard } from './app.guard';
import { LoginComponent } from './componentes/login/login.component';
import { RestablecerContraseniaComponent } from './componentes/restablecer-contrasenia/restablecer-contrasenia.component';


const routes: Routes = [
  {path:'', component: LoginComponent},
  {path: 'dashboard',canActivate: [AppGuard],component: DashboardComponent},
  {path: 'forgotpassword', component:RestablecerContraseniaComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
