import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { FirestoreService } from 'src/app/servicios/firestore.service';


@Component({
  selector: 'app-restablecer-contrasenia',
  templateUrl: './restablecer-contrasenia.component.html',
  styleUrls: ['./restablecer-contrasenia.component.css']
})
export class RestablecerContraseniaComponent implements OnInit {
  registroForm: FormGroup;
  private mensaje: string;
  private 

  constructor(private router:Router,private formBuilder: FormBuilder,private firestoreService: FirestoreService) { 
  
}
  ngOnInit() {
    this.registroForm=this.formBuilder.group({
      email:['',[Validators.required,Validators.email]],
      
    });
  }
  Olvidar(){
    if(this.comprobarCampos()){
      let email= this.registroForm.get('email').value;
      let devuelve=this.firestoreService.sendPasswordResetEmail(email);
      var error = (document.getElementById('error') as HTMLDivElement);
      error.style.display = "inline-block";
      this.mensaje= "Se ha enviado un correo para restablecer su contraseña, si el correo es valido";
    }
  }
  comprobarCampos(): boolean{
    var result: boolean =true;
    var email=(document.getElementById('email') as HTMLInputElement);
    if(this.registroForm.get('email').invalid){
      if(this.registroForm.get('email').errors.required){
        email.value="";
        email.placeholder="Email";
        email.style.setProperty("--c", "red");
        result=false;
      }
      if(this.registroForm.get('email').errors.email){
        email.value="";
        email.placeholder="Email no valido";
        email.style.setProperty("--c", "red");
        result=false;
      }
    }
      
    return result
  }
  login(){
    this.router.navigate([""]);
  }

}
