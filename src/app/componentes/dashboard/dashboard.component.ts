import { Component, OnInit } from '@angular/core';
import { FirestoreService } from 'src/app/servicios/firestore.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  private historial: Map<string, any>;
  private vector = [];
  private datos;
  private TituloDia;
  private dia;
  constructor(private firestoreService : FirestoreService) {
    this.historial = new Map();
    let date= new Date();
    let options = {
      weekday: 'long',
    };
    this.TituloDia= date.toLocaleString('es-MX', options);
    let options1 = {
      year: 'numeric', month: 'long', day: 'numeric'
    };
    this.dia=date.toLocaleString('es-MX', options1);
  }
  ngOnInit() {
    this.subDatos();
    this.subHistorial();
  }
  public subDatos(){
    this.firestoreService.getDataUser().subscribe( (user)=> {
      this.datos=user.payload.data();
      console.log(this.datos);
    })
  }
  public subHistorial() {
    this.vector = [];
    this.firestoreService.getHirtorial().subscribe((vecHistorial) => {
      vecHistorial.forEach((record: any) => {
        let id = record.payload.doc.id;
        let data = record.payload.doc.data();
        this.historial.set(id as string, data);
      });
      let date = new Date();
      let date1 = new Date();
      date1.setMonth(date1.getMonth() - 1);
      let terminado = false;
      let i = 0;
      while (!terminado && date1 < date) {
        let options = {
          weekday: 'long',
        };
        let nombreDia = date1.toLocaleString('es-MX', options);
        if (nombreDia == "lunes") {
          let ndias = 0;
          let mediah: number=0;
          let mediamin: number=0;

          //tengo el primer dia de la semana contar los dias de entrenamiento hasta el domingo
          for (let i of this.historial.values()) {
            let fHist = new Date(i.fecha.seconds * 1000);
            let date2 = new Date(date1);
            date2.setDate(date2.getDate() + 6);
            if (date2 > date) {
              if (fHist >= date1 && fHist <= date) {
                if (i.dia != "Actualización Peso") {
                  ndias = ndias + 1;
                  let duracionh: string= i.Duracion;
                  let duracionmin: string=i.Duracion;
                  let y=duracionh.slice(0,1);
                  let p=duracionmin.slice(3,duracionmin.length);
                  let p1=p.split("min");
                  mediah=mediah+parseInt(y);
                  console.log(mediah);
                  mediamin=mediamin+parseInt(p1[0]);
                }
              }
            } else {
              if (fHist >= date1 && fHist <= date2) {
                if (i.dia != "Actualización Peso") {
                  ndias = ndias + 1;
                  let duracionh: string= i.Duracion;
                  let duracionmin: string=i.Duracion;
                  let y=duracionh.slice(0,1);
                  let p=duracionmin.slice(3,duracionmin.length);
                  let p1=p.split("min");
                  mediah=mediah+parseInt(y);
                  mediamin=mediamin+parseInt(p1[0]);
                }
              }
            }
          }
          let options = {
            day: "2-digit",
            month: "2-digit",
          };
          let f = date1.toLocaleString('es-MX', options);
          let f2=new Date(date1.setDate(date1.getDate()+6));
          let f1=f2.toLocaleString('es-MX', options);
          //calculo total;
          let horas=mediah*60;
          mediamin=mediamin+horas;
          let media=mediamin;
          let tiemo=(mediamin/60);
          let total=tiemo.toString().split(".");
          let min= tiemo-parseInt(total[0]);
          min=Math.round(min*60);
          //calculo media
          if(ndias!=0){
            media=media/ndias;
            let tiemomedia=(media/60);
            let totalmedia=tiemomedia.toString().split(".");
            let minmedia= tiemomedia-parseInt(totalmedia[0]);
            minmedia=Math.round(minmedia*60);
  
            let barra = {
              d:f+" - "+f1,
              num: ndias,
              media:""+totalmedia[0]+"h "+minmedia+"min ",
              total:""+total[0]+"h "+min+"min ",
            }
            this.vector.push(barra);
          }else{
           
            let barra = {
              d: f+" - "+f1,
              num: ndias,
              media:" - ",
              total:" - ",
            }
            this.vector.push(barra);
          }
        }
        if (date1 == date) {
          terminado = true;
        }
        date1.setDate(date1.getDate() + 1);
      }
    });
  }
  salir(){
    this.firestoreService.logout();
  }
}
