import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { FormGroup, Validators, FormControl, FormBuilder } from '@angular/forms';
import { FirestoreService } from 'src/app/servicios/firestore.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, AfterViewInit {
  loginForm: FormGroup;
  mensajeError;
  errorMessage;
  successMessage;
  constructor(private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private firestore: FirestoreService) {
  }
  ngAfterViewInit() {
  }

  ngOnInit() {
    this.isLoggedIn();
    //this.firestore.logout();
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
    var error = (document.getElementById('error') as HTMLDivElement);
    error.style.display = "none";
  }
  get f() { return this.loginForm.controls; }
  onLogin(form) {
    if (this.comprobarCampos()) {
      this.firestore.login(form.email, form.password)
        .then(res => {
          this.comprobandoLogin();
        }, err => {
          //Si hay algun eror de contraseña, email invalido o cualquier otra cosa
          var error = (document.getElementById('error') as HTMLDivElement);
          error.style.display = "inline-block";
          this.mensajeError = "Email o contraseña invalido";
        })
    }
  }
  comprobandoLogin() {
    this.firestore.isAuth().subscribe(user => {
      if (user) {
        if (user.emailVerified) {
          // esto debe de hacerlo el registro
          localStorage.setItem("logueado", "true");
          this.router.navigate(['dashboard']);
        } else {
          var error = (document.getElementById('error') as HTMLDivElement);
          error.style.display = "inline-block";
          this.mensajeError = "Verifica su correo electronico";
        }
      } else {
      }
    });
  }
  comprobarCampos(): boolean {
    var result: boolean = true;
    var email = (document.getElementById('email') as HTMLInputElement);
    var p1 = (document.getElementById('p1') as HTMLInputElement);
    if (this.loginForm.get('email').invalid) {
      email.value = "";
      email.placeholder = "Email";
      email.style.setProperty("--c", "red");
      result = false;
    }
    if (this.loginForm.get('password').invalid) {
      p1.placeholder = "Contraseña";
      p1.style.setProperty("--c", "red");
      result = false;
    }
    return result;
  }
  isLoggedIn() {
    let logueado = localStorage.getItem("logueado");
    if (logueado == "true") {
      this.firestore.isAuth().subscribe(auth => {
        if (auth) {
          this.router.navigate(['dashboard']);
        } else {
        }
      });
    }
  }



  login() {
    localStorage.setItem('isLoggedIn', 'true');
    this.router.navigate(['dashboard']);
  }

  restablecerContrasenia() {
    this.router.navigate(['forgotpassword']);
  }

}
