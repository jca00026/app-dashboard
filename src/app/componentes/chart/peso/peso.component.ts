
import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { ChartDataSets, ChartOptions } from 'chart.js';
import { Label, Color, BaseChartDirective } from 'ng2-charts';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { ViewEncapsulation } from '@angular/core';
import {animate,keyframes,query,stagger,state,style,transition,trigger} from '@angular/animations';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FirestoreService } from 'src/app/servicios/firestore.service';

@Component({
  selector: 'app-peso',
  templateUrl: './peso.component.html',
  styleUrls: ['./peso.component.css']
})
export class PesoComponent implements OnInit {

  meta: string;

  private eli:boolean;
  private contpeso;
  private historialPesos:Map<string,any>;
  private vectPesos=[];
  private user;
  private add:string;
  @ViewChild(BaseChartDirective,{ static: false }) chart: BaseChartDirective
  public variable: any;
  constructor(private router: Router, private firestoreService:FirestoreService, private route: ActivatedRoute) { 
    //BaseChartDirective.registerPlugin(annotations);
    this.historialPesos=new Map();

    this.contpeso=0;
    this.variable=50;
    this.eli=false;
  }

  ngOnInit() {
    this.meta="-";
    this.firestoreService.getDataUser().subscribe((usuario)=>{
      this.user={
        datos: usuario.payload.data(),
      };
        this.meta=this.user.datos.Meta;
        this.subHistorialPesos();
      
    });
 
}
  public lineChartData: ChartDataSets[] = [
    { data: [], label: 'Peso' },
    { data: [], label: 'Meta' }
  ];
  public lineChartLabels: Label[] = [];

  public lineChartColors: Color[] = [
    {
      borderColor: 'mediumblue',
      backgroundColor: 'rgb(55, 55, 230,0.3)',
     
    },
    {
      borderColor: '#8400FF',
      backgroundColor: 'rgb(157, 80, 230,0.3)',
      pointRadius:0,
    },
  ];
  public lineChartOptions= {
    
    elements: {
      point: {
          radius: 4,
          borderWidth: 2,
          hoverRadius:6,
          hoverBorderWidth:3
      }
  },
   scales:
    { 
      
     xAxes: [
       { 
        ticks:{
          maxTicksLimit: 5,
          display: false,
        },
       }
     ],
     yAxes:[
      { 
        ticks:{
          suggestedMin: 60,
        },
      }
    ] 
  },/*
  annotation: {
    annotations: [
      {
        type: 'line',
        mode: 'horizontal',
        scaleID: 'y-axis-0',
        value: 75,
        borderColor: '#8400FF',
        backgroundColor: 'rgb(157, 80, 230,0.3)',
        borderWidth: 2,
      
      },
    ],
  },
*/
  };
  
  public lineChartLegend = true;
  public lineChartType = 'line';
  public lineChartPlugins = [];

  fabButtons = [
    {
      label: "Meta"
    },
    {
      label: "Peso"
    },
  ];
  buttons = [];
  fabTogglerState = 'inactive';

  subHistorialPesos(){
    this.firestoreService.getHitorialPesos().subscribe((pesos)=>{
      this.historialPesos.clear();
      this.vectPesos=[];
      pesos.forEach((p: any)=>{
        let _fecha= new Date(p.payload.doc.data().Fecha.seconds * 1000);
        let _id=p.payload.doc.id;
        let data={
          id:p.payload.doc.id,
          idHistorial:p.payload.doc.data().IdHistorial,
          peso:p.payload.doc.data().Peso,
          fecha: _fecha,
          elimianr:false,
        }
        this.historialPesos.set(_id, data);
        this.vectPesos.push(data);
      })
      this.vectPesos.sort((n1,n2)=>{
        if(new Date(n1.fecha)<new Date(n2.fecha)) return 1;
        if(new Date(n1.fecha)>new Date(n2.fecha)) return -1;
        return 0;
      });
      for(let i of this.vectPesos){
        
        let _fecha= new Date(i.fecha);
        let options = {
          day:"2-digit",
          month: "2-digit",
          year:"numeric",
        };
        let f= _fecha.toLocaleString('es-MX',options);
        i.fecha=f;
      }
      this.dibujar();
    })
  }
  dibujar(){
    this.lineChartData[0].data=[];
    this.lineChartLabels=[];
    this.lineChartData[1].data=[];
    this.lineChartData[1].data.push(parseFloat(this.meta));
    this.lineChartData[1].data.push(parseFloat(this.meta));
    let cont=0;
    let copia =this.vectPesos.slice();
    copia.reverse();
    let f;
   for(let i of copia){
     f=i.fecha;
      cont++;
      if(cont< this.contpeso){
        this.lineChartData[0].data.push(i.peso);
        this.lineChartLabels.push(i.fecha);
      }else{
        this.lineChartData[0].data.push(i.peso);
        this.lineChartLabels.push(i.fecha);
        this.lineChartData[1].data.push(parseInt(this.meta));
      }
      
     
    }
    this.lineChartLabels.push(f);
    this.chart.chart.update();
  
    
  }
  dibujarMeta(){
    if(this.contpeso==0){
      this.lineChartData[1].data.push(parseInt(this.meta));
      this.lineChartData[1].data.push(parseInt(this.meta));
      this.contpeso=2;
    }else{
      for(let i of this.lineChartData[1].data){
        i=parseInt(this.meta);
      }

    }
    let valor=parseInt(this.meta)-10;
    this.chart.chart.options.scales.yAxes[0].ticks.suggestedMin=valor;
    
    this.chart.chart.update();
    
  }
 
}
