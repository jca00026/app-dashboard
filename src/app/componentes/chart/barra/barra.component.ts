import { Component, OnInit, ViewChild } from '@angular/core';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Color, Label, BaseChartDirective } from 'ng2-charts';
import { FirestoreService } from 'src/app/servicios/firestore.service';
@Component({
  selector: 'app-barra',
  templateUrl: './barra.component.html',
  styleUrls: ['./barra.component.css']
})
export class BarraComponent implements OnInit {
  @ViewChild(BaseChartDirective, { static: false }) chart: BaseChartDirective

  private historial: Map<string, any>;
  private vector = [];
  constructor(private firestoreService: FirestoreService) {
    this.historial = new Map();
  }

  ngOnInit(): void {
    this.subHistorial();
  }
  public barChartColors: Color[] = [
    { borderColor: 'mediumblue', backgroundColor: '#8400ff' }

  ]
  barChartOptions: ChartOptions = {
    responsive: true,
    scales:
    {
      yAxes: [
        {
          ticks: {
            suggestedMin: 0,
            suggestedMax: 7,
          },
        }
      ]
    }
  };
  barChartLabels: Label[] = [];
  barChartType: ChartType = 'bar';
  barChartLegend = true;
  barChartPlugins = [];

  barChartData: ChartDataSets[] = [
    { data: [], label: 'Dias' }
  ];


  public subHistorial() {
    this.vector = [];
    this.firestoreService.getHirtorial().subscribe((vecHistorial) => {
      vecHistorial.forEach((record: any) => {
        let id = record.payload.doc.id;
        let data = record.payload.doc.data();
        this.historial.set(id as string, data);
      });
      let date = new Date();
      let date1 = new Date();
      date1.setMonth(date1.getMonth() - 1);
      let terminado = false;
      let i = 0;
      while (!terminado && date1 < date) {
        let options = {
          weekday: 'long',
        };
        let nombreDia = date1.toLocaleString('es-MX', options);
        if (nombreDia == "lunes") {
          let ndias = 0;
          //tengo el primer dia de la semana contar los dias de entrenamiento hasta el domingo
          for (let i of this.historial.values()) {
            let fHist = new Date(i.fecha.seconds * 1000);

            let date2 = new Date(date1);
            date2.setDate(date2.getDate() + 6);
            if (date2 > date) {
              if (fHist >= date1 && fHist <= date) {
                if (i.dia != "Actualización Peso") {
                  ndias = ndias + 1;
                }
              }
            } else {
              if (fHist >= date1 && fHist <= date2) {
                if (i.dia != "Actualización Peso") {
                  ndias = ndias + 1;
                }
              }
            }
          }
          let options = {
            day: "2-digit",
            month: "2-digit",
          };
          let f = date1.toLocaleString('es-MX', options);
          let barra = {
            d: f,
            num: ndias,
          }
          this.vector.push(barra);
        }
        if (date1 == date) {
          terminado = true;
        }
        date1.setDate(date1.getDate() + 1);
      }
      for (let i = 0; i < this.vector.length; i++) {
        this.barChartData[0].data.push(this.vector[i].num);
        let s = "Semana del " + this.vector[i].d;
        this.barChartLabels.push(s);
      }
      this.chart.chart.update();
    });
  }
}
