import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './servicios/auth.service';


@Injectable({
  providedIn: 'root'
})
export class AppGuard implements CanActivate {

  constructor(public router:Router, private authService: AuthService){}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      var logueado:boolean;
      logueado=this.authService.isLoggedIn;
    if(logueado){
      return true;
    }
  }
}
